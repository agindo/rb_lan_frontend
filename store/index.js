import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      counter: 0,
      berita: {}
    }),
    mutations: {
      increment (state) {
        state.counter++
      },
      GET_BERITA(state, news) {
        state.berita = news
      }
    },
    actions: {
        async getBerita ({commit}) {
            let news = await this.$axios.$get('http://103.123.230.115:1092/frontend/berita')
            commit('GET_BERITA', news.data)
        },
        getgambar(gambar){
            return new Promise((resolve,reject)=>{
                this.$axios.$post('http://103.123.230.115:1092/frontend/gambarcom/'+gambar)
                // Axios.post(ipz+'frontend/gambarcom/'+gbr)
                .then(res => {
                    resolve(res)
                })
            })
        },
    }
  })
}

export default createStore